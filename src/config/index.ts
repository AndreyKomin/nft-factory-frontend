// export const ZeroAddress = "T9yD14Nj9j7xAB4dbGeiX9h8unkKHxuWwb"; // for TRON
export const ZeroAddress = "0x0000000000000000000000000000000000000000"; // for TRON

//1155
export const TradeContractAddress1155 =
  "0xC30e9c230f782a7A96b8c615f1276564F3d70724";

export const TradeContractAddress721 =
  "0x09801CF826d876E6cc2aa32f6127b099C8D0EA2C";
// export const NFTFactoryAddress = "0x8aCD3eA03e5a7B27421966DEd0A9fA03f622B800"; // old factory without minting
export const FactoryAddress = "0x1332358ee095635ec7e1d37cc86afaa5b0421c01";

export const AlchemyNetwork = "https://polygon-mainnet.g.alchemyapi.io/nft/v2";
export const AlchemyApiKey = "oIuJ_nR4OrzlYvl9AY1mHndkQgdHcF_7";

export const networkParams = {
  "0x63564c40": {
    chainId: "0x63564c40",
    rpcUrls: ["https://api.harmony.one"],
    chainName: "Harmony Mainnet",
    nativeCurrency: { name: "ONE", decimals: 18, symbol: "ONE" },
    blockExplorerUrls: ["https://explorer.harmony.one"],
    iconUrls: ["https://harmonynews.one/wp-content/uploads/2019/11/slfdjs.png"],
  },
  "0xa4ec": {
    chainId: "0xa4ec",
    rpcUrls: ["https://forno.celo.org"],
    chainName: "Celo Mainnet",
    nativeCurrency: { name: "CELO", decimals: 18, symbol: "CELO" },
    blockExplorerUrl: ["https://explorer.celo.org"],
    iconUrls: [
      "https://celo.org/images/marketplace-icons/icon-celo-CELO-color-f.svg",
    ],
  },
};
