import { Navigate } from "react-router-dom";

const Home = () => {
  return (
    <>
      <Navigate to="/factory" />
      <h1>Home</h1>
    </>
  );
};

export default Home;
