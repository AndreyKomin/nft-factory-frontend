import { Route, Routes, useParams } from "react-router-dom";
import React, { useContext, useEffect } from "react";
import Marketplace from "./components/Marketplace";
import NftPage from "./components/NftPage";
import MyCollection from "./components/MyCollection";
import ClientProvider, { ClientContext, TClientContext } from "./ClientContext";
import LoadingLayout from "../../components/LoadingLayout";

const Collection = () => {
  const {
    actions: { setContractAddress },
    data: { isLoading },
  } = useContext(ClientContext) as TClientContext;

  const params = useParams();

  const { contractAddress } = params;
  useEffect(() => {
    setContractAddress(contractAddress);
  });

  return isLoading ? (
    <LoadingLayout />
  ) : (
    <Routes>
      {/*shows all orders by contract*/}
      <Route path="/marketplace" element={<Marketplace />} />

      {/*trading your contract window*/}
      <Route path="/:tokenId" element={<NftPage />} />

      {/*shows all your orders*/}
      <Route path="/my-collection" element={<MyCollection />} />
    </Routes>
  );
};

const Client = () => (
  <ClientProvider>
    <Routes>
      <Route path="/:contractAddress/*" element={<Collection />} />
    </Routes>
  </ClientProvider>
);

export default Client;
