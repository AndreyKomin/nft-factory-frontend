import React, { useContext, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import LoadingLayout from "../../../components/LoadingLayout";
import { ClientContext, TClientContext } from "../ClientContext";
import s from "../../../styles/modules/Client/Marketplace.module.scss";
import briefcaseIcon from "../../../assets/images/briefcase.svg";
import TokenCard from "../../../components/TokenCard/TokenCard";

const Marketplace = () => {
  const {
    actions: { getOrdersMeta },
    data: { orders, isLoading, ordersWithMetaData, contractMeta },
  } = useContext(ClientContext) as TClientContext;
  useEffect(() => {
    getOrdersMeta(orders).then();
  }, [orders.length]);
  const { contractAddress } = useParams();
  return (
    <div>
      {isLoading ? (
        <LoadingLayout />
      ) : (
        <>
          <div className={s.subheader}>
            <div className={s.title}>
              <img src={briefcaseIcon} alt={"briefcaseIcon"} />
              <h1>Markeplace</h1>
            </div>
            <div className={s.filters}>
              <div className={s.searchInput}>
                <input />
              </div>

              <div className={s.priceSection}>
                <select className={s.coinSelect}>
                  <option defaultChecked>ETH</option>
                </select>
                <input className={s.priceInput} />
                <span>&mdash;</span>
                <input className={s.priceInput} />
              </div>
              <select className={s.priceSortSelect}>
                <option defaultChecked>Low to high</option>
              </select>
            </div>
          </div>
          <div className={s.collectionGrid}>
            {contractMeta && ordersWithMetaData.length > 0 ? (
              ordersWithMetaData.map((order: any) => <TokenCard data={order} />)
            ) : (
              <p>There are no orders yet.</p>
            )}
          </div>
        </>
      )}
    </div>
  );
};

export default Marketplace;
