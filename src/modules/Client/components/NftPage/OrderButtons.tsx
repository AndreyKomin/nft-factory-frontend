// import s from "../../../../styles/modules/Client/TradeNft.module.scss";
// import React, { useContext } from "react";
// import { ClientContext, TClientContext } from "../../ClientContext";
//
const OrderButtons = ({
  buttonsVisibility: {
    isOnSale,
    isItOwner,
    isSellerMe,
    isSellerAccepted,
    isBuyerExists,
    isBuyerMe,
  },
  actions: {
    redeemOrderAction,
    removeOrderAction,
    addOrderAction,
    initializeOrderAction,
    acceptOrderAction,
    declineOrderAction,
  },
}: any) => {
  //   // const {
  //   //   actions: {
  //   //     addOrder,
  //   //     acceptOrder,
  //   //     declineOrder,
  //   //     removeOrder,
  //   //     redeemOrder,
  //   //     initializeOrder,
  //   //   },
  //   //   data: { account, isLoading, contractMeta },
  //   // } = useContext(ClientContext) as TClientContext;
  //   return (
  //     <div className={s.buttons}>
  //       {isOnSale && !isSellerMe && (
  //         <div className={s.actions}>
  //           {isBuyerMe && !isSellerAccepted && (
  //             <p className={s.message}>Waiting for seller accept...</p>
  //           )}
  //           {!isBuyerMe && !isSellerAccepted && (
  //             <button
  //               className={s.buttonPrimaryColored}
  //               onClick={() =>
  //                 redeemOrder(
  //                   parseInt(orderData[0].orderId._hex),
  //                   parseInt(orderData[0].priceWEI._hex)
  //                 )
  //               }
  //             >
  //               Buy now
  //             </button>
  //           )}
  //         </div>
  //       )}
  //       {isOnSale && !isSellerAccepted && !isBuyerExists && isSellerMe && (
  //         <button
  //           onClick={() => removeOrder(parseInt(orderData[0].orderId._hex))}
  //           className={s.buttonPrimaryColored}
  //         >
  //           Remove Order
  //         </button>
  //       )}
  //       {isOnSale && !isSellerAccepted && isBuyerExists && (
  //         <button
  //           className={s.buttonPrimaryColored}
  //           onClick={() => declineOrder(parseInt(orderData[0].orderId._hex))}
  //         >
  //           Decline order
  //         </button>
  //       )}
  //       {isBuyerExists && isSellerMe && !isSellerAccepted && (
  //         <>
  //           <button
  //             className={s.buttonPrimaryColored}
  //             onClick={() => acceptOrder(parseInt(orderData[0].orderId._hex))}
  //           >
  //             Accept order
  //           </button>
  //         </>
  //       )}
  //       {isSellerAccepted && (
  //         <div className={s.actions}>
  //           {isBuyerMe && !isSellerMe && isSellerAccepted && (
  //             <p className={s.message}>
  //               Seller accepted offer. You can now complete your order!
  //             </p>
  //           )}
  //           {!isBuyerMe && isSellerAccepted && isSellerMe && (
  //             <p className={s.message}>
  //               You accepted offer. You can now complete your order!
  //             </p>
  //           )}
  //           <button
  //             className={s.buttonPrimaryColored}
  //             onClick={() => initializeOrder(parseInt(orderData[0].orderId._hex))}
  //           >
  //             Complete order
  //           </button>
  //         </div>
  //       )}
  //
  //       <div className={s.transactionForm}>
  //         <div>
  //           <label style={{ marginRight: 16 }}>Price WEI</label>
  //           <input
  //             placeholder={"10000"}
  //             className={s.textInput}
  //             onChange={(e) => {
  //               setPriceWEI(Number(e.target.value));
  //             }}
  //           />
  //         </div>
  //         <button
  //           className={s.buttonPrimaryColored}
  //           onClick={() => addOrder({ priceWEI }, tokenId)}
  //         >
  //           Sell
  //         </button>
  //       </div>
  //     </div>
  //   );
};
export default OrderButtons;
