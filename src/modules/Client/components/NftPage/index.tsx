import React, { useCallback, useContext, useEffect, useState } from "react";
import { Box, Button, Form, FormField, Heading, Text } from "grommet";
import s from "../../../../styles/modules/Client/TradeNft.module.scss";
import LoadingLayout from "../../../../components/LoadingLayout";
import { useLocation, useParams } from "react-router-dom";
import Orders from "../Orders";
import { ClientContext, TClientContext } from "../../ClientContext";
import { MainContext, TMainContext } from "../../../../context/MainContext";
import { getNFTMetadata } from "../../../../data/alchemy";
import TradeNft1155 from "./TradeNft1155";
import TradeNft721 from "./TradeNft721";

type TProps = {
  contractAddress: string;
  tokenId: string;
};

type OrderObject = {
  amount: { _hex: string };
  buyer: string;
  orderId: { _hex: string };
  percentFee: { _hex: string };
  priceWEI: { _hex: string };
  seller: string;
  sellerAccepted: boolean;
  tokenId: { _hex: string };
};

interface FormState {
  NFTAddress?: string;
  tokenID?: number;
  amount?: number;
  priceWEI?: number;
  secondsToEnd?: number;
}

const NftPage = () => {
  const {
    actions: { setLoading, getAllOrdersByToken, getOwner, setActiveTokenId },
    data: {
      contractBaseUri,
      ordersByToken,
      isLoading,
      contractMeta,
      is1155,
      ownerOfToken,
      collectionInstance,
    },
  } = useContext(ClientContext) as TClientContext;

  const params = useParams();
  const { contractAddress, tokenId } = params;
  const [tokenMetaData, setTokenMetaData] = useState<any>();
  const [imgSrc, setSrc] = useState<any>();

  useEffect(() => {
    if (tokenId) {
      setActiveTokenId(tokenId);
    }

    return () => {
      setActiveTokenId(null);
    };
  }, [tokenId]);

  const getSrc = useCallback(async () => {
    const src = contractMeta?.contractMetadata?.baseUri + `${tokenId}.png`;
    setSrc(src);
  }, [contractMeta, collectionInstance, is1155]);

  useEffect(() => {
    setLoading(true);
    if (!tokenMetaData?.contractMetadata) {
      getNFTMetadata(contractAddress, Number(tokenId)).then((data) => {
        setTokenMetaData(data);
        setLoading(false);
      });
    }
    getOwner(tokenId).then();

    getAllOrdersByToken(tokenId).then();

    setLoading(false);
  }, [contractAddress, tokenId, contractMeta]);

  useEffect(() => {
    getSrc().then();
  }, [collectionInstance, contractMeta]);

  return (
    <>
      <>
        {is1155 ? (
          <TradeNft1155
            orderData={ordersByToken}
            tokenMetaData={tokenMetaData}
            imgSrc={imgSrc}
            ownerOfToken={ownerOfToken}
          />
        ) : (
          <TradeNft721
            orderData={ordersByToken}
            tokenMetaData={tokenMetaData}
            imgSrc={imgSrc}
            ownerOfToken={ownerOfToken}
          />
        )}
      </>
    </>
  );
};

export default NftPage;
