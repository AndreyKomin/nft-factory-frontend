import React, { useCallback, useContext, useEffect, useState } from "react";
import s from "../../../../styles/modules/Client/TradeNft.module.scss";
import LoadingLayout from "../../../../components/LoadingLayout";
import { useLocation, useParams } from "react-router-dom";
import Orders from "../Orders";
import { ClientContext, TClientContext } from "../../ClientContext";
import { getNFTMetadata, getOwnerOfToken } from "../../../../data/alchemy";

const TradeNft1155 = ({
  imgSrc,
  tokenMetaData,
  orderData,
  ownerOfToken,
}: any) => {
  const {
    actions: { addOrder },
    data: { isLoading, tokenAmount, account, contractMeta },
  } = useContext(ClientContext) as TClientContext;

  const params = useParams();
  const { contractAddress, tokenId } = params;
  const [isItOwner, setOwner] = useState<boolean>(false);
  const [amount, setAmount] = useState<number>(0);
  const [priceWEI, setPriceWEI] = useState<number>(0);
  const [transactionForm, setTransactionForm] = useState<any>(false);

  const getOwner = useCallback(async () => {
    await getOwnerOfToken(contractAddress, tokenId).then((data) => {
      const owners = data.owners.map((item) => {
        return item.toLowerCase();
      });
      setOwner(owners.includes(account.toLowerCase()));
    });
  }, [contractAddress, tokenId, account]);

  useEffect(() => {
    getOwner().then();
  });
  // const startAuctionAction = async () => {
  //   const { priceWEI, amount, secondsToEnd } = value;
  //   await startAuction(
  //     contractAddress,
  //     tokenId,
  //     Number(amount),
  //     priceWEI,
  //     secondsToEnd
  //   );
  // };
  return (
    <>
      {isLoading && !tokenMetaData ? (
        <>
          <p>Do not refresh page please. Waiting for transaction done.</p>
          <LoadingLayout />
        </>
      ) : (
        <>
          <div className={s.tradeContract}>
            <div className={s.card}>
              {imgSrc && <img className={s.img} src={imgSrc} />}
              <div className={s.info}>
                {tokenMetaData && (
                  <>
                    <h1>{contractMeta.contractMetadata.name}</h1>
                    <p>#{tokenId}</p>
                    <p>Floor Price: 0</p>
                    {isItOwner && <p>Amount: {tokenAmount}</p>}
                  </>
                )}
                {transactionForm && (
                  <div className={s.transactionForm}>
                    <div style={{ marginBottom: "15px" }}>
                      <label style={{ marginRight: "10px" }}>Amount</label>
                      <input
                        className={s.textInput}
                        onChange={(e) => {
                          setAmount(Number(e.target.value));
                        }}
                      />
                    </div>
                    <div style={{ marginBottom: "15px" }}>
                      <label style={{ marginRight: "10px" }}>Price WEY</label>
                      <input
                        className={s.textInput}
                        onChange={(e) => {
                          setPriceWEI(Number(e.target.value));
                        }}
                      />
                    </div>
                    <button
                      className={s.buttonPrimaryColored}
                      onClick={() =>
                        addOrder({ amount, priceWEI }, tokenId, contractAddress)
                      }
                    >
                      Sell
                    </button>
                  </div>
                )}
                <div className={s.buttons}>
                  {!transactionForm && isItOwner && (
                    <button
                      className={s.buttonPrimaryColored}
                      onClick={() => setTransactionForm(true)}
                    >
                      Sell
                    </button>
                  )}
                </div>
              </div>
            </div>
            <div>
              <h2>Description</h2>
              {!!tokenMetaData && (
                <p>{tokenMetaData.description || "No description yet"}</p>
              )}
            </div>
            <div>
              <h2>{isItOwner ? "Orders" : "Offers"}</h2>
              <Orders orders={orderData} contractAddress={contractAddress} />
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default TradeNft1155;
