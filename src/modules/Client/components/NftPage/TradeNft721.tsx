import React, { useCallback, useContext, useEffect, useState } from "react";
import s from "../../../../styles/modules/Client/TradeNft.module.scss";
import { useParams } from "react-router-dom";
import { ClientContext, TClientContext } from "../../ClientContext";
import { ZeroAddress } from "../../../../config";
import LoadingLayout from "../../../../components/LoadingLayout";
import { getOwnerOfToken } from "../../../../data/alchemy";
import OrderButtons from "./OrderButtons";

type TProps = {
  contractAddress: string;
  tokenId: string;
};

type OrderObject = {
  amount: { _hex: string };
  buyer: string;
  orderId: { _hex: string };
  percentFee: { _hex: string };
  priceWEI: { _hex: string };
  seller: string;
  sellerAccepted: boolean;
  tokenId: { _hex: string };
};

interface FormState {
  NFTAddress?: string;
  tokenID?: number;
  amount?: number;
  priceWEI?: number;
  secondsToEnd?: number;
}
const sell721 = [{ label: "Price WEI", valueField: "priceWEI" }];
const TradeNft721 = ({
  imgSrc,
  tokenMetaData,
  orderData,
  ownerOfToken,
}: any) => {
  const {
    actions: {
      addOrder,
      acceptOrder,
      declineOrder,
      removeOrder,
      redeemOrder,
      initializeOrder,
    },
    data: { account, isLoading, contractMeta },
  } = useContext(ClientContext) as TClientContext;

  const params = useParams();
  const { contractAddress, tokenId } = params;
  const getTransactionForm = (form: string) => {};
  const [priceWEI, setPriceWEI] = useState<number>(0);

  const [transactionForm, setTransactionForm] = useState<any>(false);
  const [isItOwner, setOwner] = useState<boolean>(false);

  const isSellerMe = account === orderData[0]?.seller;
  const isOnSale = !!orderData[0];
  const isSellerAccepted = orderData[0]?.sellerAccepted;

  const isBuyerExists = orderData[0]?.buyer !== ZeroAddress;

  const isBuyerMe = account === orderData[0]?.buyer;
  const getOwner = useCallback(async () => {
    await getOwnerOfToken(contractAddress, tokenId).then((data) => {
      const owners = data.owners.map((item) => {
        return item.toLowerCase();
      });
      setOwner(owners.includes(account.toLowerCase()));
    });
  }, [contractAddress, tokenId, account]);

  useEffect(() => {
    getOwner().then();
  });
  // const redeemOrderAction = () => {};
  // const removeOrderAction = () => {};
  // const addOrderAction = () => {};
  // const initializeOrderAction = () => {};
  // const acceptOrderAction = () => {};
  // const declineOrderAction = () => {};
  // const startAuctionAction = async () => {
  //   const { priceWEI, amount, secondsToEnd } = value;
  //   await startAuction(
  //     contractAddress,
  //     tokenId,
  //     Number(amount),
  //     priceWEI,
  //     secondsToEnd
  //   );
  // };
  return (
    <>
      {isLoading && !tokenMetaData ? (
        <>
          <p>Do not refresh page please. Waiting for transaction done.</p>
          <LoadingLayout />
        </>
      ) : (
        <div className={s.tradeContract}>
          <div className={s.card}>
            {!!imgSrc && <img className={s.img} src={imgSrc} />}

            <div className={s.info}>
              {!!contractMeta && (
                <>
                  <h1>{contractMeta.contractMetadata.name}</h1>
                  <p>Status: {isOnSale ? "On Sale" : "Not in sale"}</p>
                  <p>#{tokenId}</p>
                  {!!orderData[0] && (
                    <div className={s.price}>
                      Price:
                      <span className={s.number}>
                        {parseInt(orderData[0].priceWEI._hex)}
                      </span>
                    </div>
                  )}
                </>
              )}

              {transactionForm && (
                <div className={s.transactionForm}>
                  <div>
                    <label style={{ marginRight: 16 }}>Price WEI</label>
                    <input
                      placeholder={"10000"}
                      className={s.textInput}
                      onChange={(e) => {
                        setPriceWEI(Number(e.target.value));
                      }}
                    />
                  </div>
                  <button
                    className={s.buttonPrimaryColored}
                    onClick={() => addOrder({ priceWEI }, tokenId)}
                  >
                    Sell
                  </button>
                </div>
              )}
              <div className={s.buttons}>
                {!isOnSale && !transactionForm && isItOwner && (
                  <button
                    className={s.buttonPrimaryColored}
                    onClick={() => setTransactionForm(true)}
                  >
                    Sell
                  </button>
                )}
                {orderData.length > 0 && (
                  <>
                    {isOnSale && !isSellerMe && (
                      <div className={s.actions}>
                        {isBuyerMe && !isSellerAccepted && (
                          <p className={s.message}>
                            Waiting for seller accept...
                          </p>
                        )}
                        {!isBuyerMe && !isSellerAccepted && (
                          <button
                            className={s.buttonPrimaryColored}
                            onClick={() =>
                              redeemOrder(
                                parseInt(orderData[0].orderId._hex),
                                parseInt(orderData[0].priceWEI._hex)
                              )
                            }
                          >
                            Buy now
                          </button>
                        )}
                      </div>
                    )}
                    {isOnSale &&
                      !isSellerAccepted &&
                      !isBuyerExists &&
                      isSellerMe && (
                        <button
                          onClick={() =>
                            removeOrder(parseInt(orderData[0].orderId._hex))
                          }
                          className={s.buttonPrimaryColored}
                        >
                          Remove Order
                        </button>
                      )}
                    {isOnSale && !isSellerAccepted && isBuyerExists && (
                      <button
                        className={s.buttonPrimaryColored}
                        onClick={() =>
                          declineOrder(parseInt(orderData[0].orderId._hex))
                        }
                      >
                        Decline order
                      </button>
                    )}
                    {isBuyerExists && isSellerMe && !isSellerAccepted && (
                      <>
                        <button
                          className={s.buttonPrimaryColored}
                          onClick={() =>
                            acceptOrder(parseInt(orderData[0].orderId._hex))
                          }
                        >
                          Accept order
                        </button>
                      </>
                    )}
                    {isSellerAccepted && (
                      <div className={s.actions}>
                        {isBuyerMe && !isSellerMe && isSellerAccepted && (
                          <p className={s.message}>
                            Seller accepted offer. You can now complete your
                            order!
                          </p>
                        )}
                        {!isBuyerMe && isSellerAccepted && isSellerMe && (
                          <p className={s.message}>
                            You accepted offer. You can now complete your order!
                          </p>
                        )}
                        <button
                          className={s.buttonPrimaryColored}
                          onClick={() =>
                            initializeOrder(parseInt(orderData[0].orderId._hex))
                          }
                        >
                          Complete order
                        </button>
                      </div>
                    )}
                  </>
                )}
              </div>
            </div>
          </div>
          <div>
            <h2>Description</h2>
            {!!tokenMetaData && (
              <p>{tokenMetaData.description || "No description yet"}</p>
            )}
          </div>
        </div>
      )}
    </>
  );
};

export default TradeNft721;
