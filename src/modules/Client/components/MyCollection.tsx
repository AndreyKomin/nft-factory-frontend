import React, { useCallback, useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { ClientContext, TClientContext } from "../ClientContext";
import s from "../../../styles/modules/Client/MyCollection.module.scss";
import briefcaseIcon from "../../../assets/images/briefcase.svg";
import TokenCard from "../../../components/TokenCard/TokenCard";

const MyCollection = () => {
  const {
    actions: { tokensCardData, getOrdersMeta },
    data: {
      myOrders,
      tokens,
      ordersWithMetaData,
      contractMeta,
      contractBaseUri,
    },
  } = useContext(ClientContext) as TClientContext;

  const [tab, setTab] = useState<string>("Not for sale");

  useEffect(() => {
    getOrdersMeta(myOrders).then();
  }, [myOrders.length, contractMeta]);

  return (
    <>
      <div className={s.subheader}>
        <div className={s.title}>
          <img src={briefcaseIcon} alt={"briefcaseIcon"} />
          <h1>My Collection</h1>
        </div>
        <div className={s.filters}>
          <button
            onClick={() => setTab("Not for sale")}
            className={
              tab === "Not for sale"
                ? s.buttonSecondaryYellow
                : s.buttonSecondary
            }
          >
            Not for sale
          </button>
          <button
            onClick={() => setTab("On sale")}
            className={
              tab === "On sale" ? s.buttonSecondaryYellow : s.buttonSecondary
            }
          >
            On sale
          </button>
        </div>
      </div>
      <div className={s.collectionGrid}>
        {contractMeta &&
          tab === "Not for sale" &&
          tokensCardData(tokens).map((token: any, index: number) => (
            <TokenCard data={token} key={`tokenCardCollection${index}`} />
          ))}
        {contractMeta &&
          tab === "On sale" &&
          ordersWithMetaData &&
          ordersWithMetaData.map((order: any, index: number) => (
            <TokenCard data={order} key={`orderCardCollection${index}`} />
          ))}
      </div>
    </>
  );
};

export default MyCollection;
