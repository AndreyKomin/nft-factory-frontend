import { Box, Text } from "grommet";
import React from "react";

const Token = ({ id }: { id: string | undefined }) => (
  <Box
    background="#6FFFB0"
    margin="xsmall"
    width="xsmall"
    height="xsmall"
    key={id}
    justify="center"
    alignContent="center"
    align="center"
  >
    <Text>
      <h2>{id}</h2>
    </Text>
  </Box>
);

export default Token;
