import React, { useContext } from "react";
import { ZeroAddress } from "../../../config";
import s from "../../../styles/modules/Client/Orders.module.scss";
import LoadingLayout from "../../../components/LoadingLayout";
import { ClientContext, TClientContext } from "../ClientContext";

type OrderObject = {
  amount: { _hex: string };
  buyer: string;
  orderId: { _hex: string };
  percentFee: { _hex: string };
  priceWEI: { _hex: string };
  seller: string;
  sellerAccepted: boolean;
  tokenId: { _hex: string };
};

interface Props {
  orders: OrderObject[];
  contractAddress: string | undefined;
}

const Orders = ({ orders, contractAddress }: Props) => {
  const {
    data: { isLoading, account },
    actions: {
      removeOrder,
      redeemOrder,
      declineOrder,
      initializeOrder,
      acceptOrder,
    },
  } = useContext(ClientContext) as TClientContext;
  console.log(orders);
  return (
    <>
      {isLoading ? (
        <LoadingLayout />
      ) : (
        <div className={s.ordersList}>
          {orders.map((order: OrderObject) => {
            const isSellerMe = account === order.seller;

            const isSellerAccepted = order.sellerAccepted;

            const isBuyerExists = order.buyer !== ZeroAddress;

            const isBuyerMe = account === order.buyer;
            console.log("isSellerMe", isSellerMe);
            console.log("isSellerAccepted", isSellerAccepted);
            console.log("isBuyerExists", isBuyerExists);
            console.log("isBuyerMe", isBuyerMe);
            return (
              <div className={s.order}>
                {order.amount && <p>Amount: {parseInt(order.amount._hex)}</p>}
                <p>Price: {parseInt(order.priceWEI._hex)}</p>
                {isSellerMe ? (
                  <p>Seller: You</p>
                ) : (
                  <p>Seller: {order.seller}</p>
                )}
                {isBuyerExists && isSellerMe && <p>Buyer: {order.buyer}</p>}
                <div className={s.buttons}>
                  {!isSellerMe && !isBuyerMe && (
                    <button
                      className={s.buttonPrimaryColored}
                      onClick={() =>
                        redeemOrder(
                          parseInt(order.orderId._hex),
                          parseInt(order.priceWEI._hex)
                        )
                      }
                    >
                      Buy Now
                    </button>
                  )}
                  {isSellerMe && isBuyerExists && !isSellerAccepted && (
                    <button
                      className={s.buttonPrimaryColored}
                      onClick={() => acceptOrder(parseInt(order.orderId._hex))}
                    >
                      Accept Order
                    </button>
                  )}
                  <>
                    {isSellerMe && !isBuyerExists && (
                      <button
                        className={s.buttonPrimaryColored}
                        color="red"
                        onClick={() =>
                          removeOrder(parseInt(order.orderId._hex))
                        }
                      >
                        Remove order
                      </button>
                    )}
                  </>
                  {(isBuyerMe || isSellerMe) && isBuyerExists && (
                    <button
                      className={s.buttonPrimaryColored}
                      onClick={() => declineOrder(parseInt(order.orderId._hex))}
                    >
                      Decline order
                    </button>
                  )}
                  {isSellerAccepted && (
                    <button
                      className={s.buttonPrimaryColored}
                      onClick={() =>
                        initializeOrder(parseInt(order.orderId._hex))
                      }
                    >
                      Complete order
                    </button>
                  )}
                </div>
              </div>
            );
          })}
        </div>
      )}
    </>
  );
};

export default Orders;
