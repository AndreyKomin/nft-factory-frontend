import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import { ethers } from "ethers";
import {
  TradeContractAddress721,
  TradeContractAddress1155,
} from "../../config";
import abi721 from "../../abi/Marketplace721.json";
import abi1155 from "../../abi/Marketplace1155.json";
import useMarketplace from "../../hooks/useMarketplace";
import { MainContext, TMainContext } from "../../context/MainContext";
import { getNFTMetadata, getNFTs } from "../../data/alchemy";
import async from "async";
import useCollection from "../../hooks/useCollection";

export type TClientContext = {
  actions: any;
  data: any;
};
export const ClientContext = createContext<TClientContext | null>(null);

const ClientProvider = ({ children }: any) => {
  const {
    actions: { setLoading, setContractAddress, setContractBaseUri },
    data: {
      signer,
      contractMeta,
      account,
      isLoading,
      userError,
      contractAddress,
      is1155,
      collectionInstance,
      contractBaseUri,
    },
  } = useContext(MainContext) as TMainContext;

  const [activeTokenId, setActiveTokenId] = useState<any>(null);
  const [tradeContractInstance, setTradeContractInstance] = useState<any>();

  const [tokens, setTokens] = useState<any[]>([]);
  const [ordersWithMetaData, setOrdersMeta] = useState<any>(false);

  const tokensCardData = (ownedTokens: any) => {
    return ownedTokens.map((token: any) => {
      const tokenId = parseInt(token.id.tokenId);
      const imgSrc = contractMeta.contractMetadata.baseUri + `${tokenId}.png`;
      const link = `/client/${contractAddress}/${tokenId}`;
      const amount = is1155 ? token.balance : "";
      return {
        contractMeta,
        imgSrc,
        link,
        amount,
        tokenId,
      };
    });
  };

  useEffect(() => {
    //setting marketplace instance
    const contract = is1155
      ? TradeContractAddress1155
      : TradeContractAddress721;
    const abi = is1155 ? abi1155.abi : abi721.abi;
    const tradeContract = new ethers.Contract(contract, abi, signer);
    setTradeContractInstance(tradeContract);
  }, [signer, is1155]);

  const { actionsMarketplace, dataMarketplace } = useMarketplace({
    contractAddress,
    activeTokenId,
    marketplaceInstance: tradeContractInstance,
    setLoading,
    is1155,
  });
  const { getAllOrders } = actionsMarketplace;

  const { collectionData, collectionActions } = useCollection({
    collectionInstance,
    setLoading,
    is1155,
    account,
  });

  const { orders } = dataMarketplace;
  const myOrders = orders.filter((order: any) => order.seller === account);

  const getOrdersMeta = useCallback(
    async (ordersArr: any) => {
      if (!ordersArr) {
        return;
      }
      const ordersData: any = [];
      await async.eachSeries(ordersArr, async (order: any, call: any) => {
        const meta = await getNFTMetadata(
          contractAddress,
          parseInt(order.tokenId._hex)
        );
        ordersData.push(meta);
        return call;
      });
      const transformedData = tokensCardData(ordersData);
      setOrdersMeta(transformedData);
    },
    [contractAddress, is1155]
  );

  useEffect(() => {
    if (!contractAddress || !account || !tradeContractInstance) {
      return;
    }
    getNFTs(account, [contractAddress]).then((data) => {
      setTokens(data.ownedNfts);
    });
    getAllOrders().then(() => {});
    setLoading(false);
  }, [contractAddress, account, tradeContractInstance]);

  return (
    <ClientContext.Provider
      value={{
        actions: {
          setLoading,
          tokensCardData,
          setContractAddress,
          getOrdersMeta,
          setActiveTokenId,
          ...actionsMarketplace,
          ...collectionActions,
        },
        data: {
          is1155,
          userError,
          account,
          isLoading,
          tokens,
          myOrders,
          collectionInstance,
          ordersWithMetaData,
          contractMeta,
          contractBaseUri,
          ...collectionData,
          ...dataMarketplace,
        },
      }}
    >
      {children}
    </ClientContext.Provider>
  );
};

export default ClientProvider;
