import { Grommet } from "grommet";
import Header from "../components/Header/Header";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import styles from "../App.module.scss";
import Home from "./Home";
import React from "react";
import { hpe } from "grommet-theme-hpe";
import { deepMerge } from "grommet/utils";
import Factory from "./Factory";
import Client from "./Client";
import Footer from "../components/Footer/Footer";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore

const theme = deepMerge(hpe, {
  global: {
    colors: {
      "blue!": "#4f8ae0",
      brand: "blue!",
      focus: "#ffde0e",
    },
  },
});

const AppLayout = () => {
  return (
    <Grommet theme={theme} full={true}>
      <div className={styles.back}>
        <BrowserRouter>
          <Header />
          <main className={styles.main}>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path={"/factory/*"} element={<Factory />} />
              <Route path={"/client/*"} element={<Client />} />
              {/*shows wallet connecting window*/}
              {/*<Route path="/client/:accountId/connect" element={<ConnectWallet />} />*/}
            </Routes>
          </main>
          <Footer />
        </BrowserRouter>
      </div>
    </Grommet>
  );
};

export default AppLayout;
