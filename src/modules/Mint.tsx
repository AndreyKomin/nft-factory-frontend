import React, { useCallback } from "react";
import { Button } from "grommet";
import useContract from "../hooks/useContract";
import { utils } from "ethers";

const { formatUnits } = utils;

const Mint = () => {
  const trc20ContractAddress = "TQ4FYQ4zT3z2q5GykPcPjswvAG3vi5WZMa";

  const {
    hasLoaded,
    instance,
    data: { name, totalSupply, baseURI },
  } = useContract(trc20ContractAddress, ["name", "totalSupply", "baseURI"]);

  const mint = useCallback(() => {
    const toAddress = "TDMu6hcooK1uLC6bzrM7cJLMMj1BrmJVoh";
    const tokenIdUint256 = totalSupply;
    const tokenURI = baseURI;

    return instance
      .mintWithTokenURI(toAddress, tokenIdUint256, tokenURI)
      .send();
  }, [baseURI, instance, totalSupply]);

  if (!hasLoaded) {
    return <div>Loading...</div>;
  }

  const formattedTotalSupply = formatUnits(totalSupply, 0);

  return (
    <div>
      <h2>{name}</h2>
      <br />
      <span>{formattedTotalSupply}</span>
      <br />
      <span>{baseURI}</span>
      <br />

      <Button primary size="medium" onClick={mint} label="Mint" />
    </div>
  );
};

export default Mint;
