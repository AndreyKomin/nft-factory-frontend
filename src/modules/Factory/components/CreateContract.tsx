import React, { useContext, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { FactoryContext, TFactoryContext } from "../FactoryContext";
import s from "../../../styles/modules/Factory/CreateContract.module.scss";
import createIcon from "../../../assets/images/edit.svg";
import CustomRadio from "../../../components/Inputs/CustomRadio";
import LoadingLayout from "../../../components/LoadingLayout";
export interface IFormState {
  standard?: string;
  name?: string;
  symbol?: string;
  baseURI?: string;
  owner?: string;
  fee?: number;
  price?: number;
  amount?: number;
  salt?: number;
  ids?: string;
  amounts?: string;
}

const CreateContract = () => {
  const navigate = useNavigate();
  const {
    actions: { buildContract, setLoading },
    data: { isLoading },
  } = useContext(FactoryContext) as TFactoryContext;

  const [formData, setFormData] = useState<IFormState>({
    standard: "ERC721",
    baseURI: "https://game-example.fra1.digitaloceanspaces.com/static/721",
  });
  const [message, setMessage] = useState<any>(false);
  const [messageErr, setMessageErr] = useState<any>(false);
  /// Shows address of new NFT contract deployed with 'Built Contract'

  const handleBuildContract = async () => {
    await buildContract(
      formData,
      () => {
        // navigate("/factory");
        setLoading(false);
        setMessage("Hurray! Great Job!");
      },
      () => {
        setLoading(false);
        setMessageErr("Something went wrong, please retry");
      }
    );

    // .catch(() => {
    //   setMessageErr("Something went wrong, please retry");
    // });
    // navigate("/factory");
  };

  return (
    <>
      {isLoading ? (
        <LoadingLayout />
      ) : (
        <div className={s.createContract}>
          <div className={s.subheader}>
            <div className={s.title}>
              <img src={createIcon} alt={"briefcaseIcon"} />
              <h1>Create Collection</h1>
            </div>
            <p>Fill the form below and check new contract address on Polygon</p>
          </div>
          <form className={s.form} onSubmit={(event) => event.preventDefault()}>
            <div className={s.standardBtns}>
              <CustomRadio
                checked={formData.standard === "ERC721"}
                label={"ERC721 (For unique items)"}
                onChangeHandle={(value: string) =>
                  setFormData({ ...formData, standard: value })
                }
                value={"ERC721"}
              />
              <CustomRadio
                checked={formData.standard === "ERC1155"}
                label={"ERC1155 (For non-unique items)"}
                onChangeHandle={(value: string) =>
                  setFormData({ ...formData, standard: value })
                }
                value={"ERC1155"}
              />
            </div>

            <div className={s.textInputs}>
              <div className={s.formInput}>
                <label className={s.formInputLabel}>
                  Name <strong>*</strong>
                </label>
                <input
                  name="name"
                  className={s.textInput}
                  required
                  onChange={(e) =>
                    setFormData({ ...formData, name: e.target.value })
                  }
                />
              </div>

              <div className={s.formInput}>
                <label className={s.formInputLabel}>
                  Symbol <strong>*</strong>
                </label>
                <input
                  onChange={(e) =>
                    setFormData({ ...formData, symbol: e.target.value })
                  }
                  name="symbol"
                  className={s.textInput}
                  required
                  // validate={{ regexp: /^[a-z]/i }}
                />
              </div>

              <div className={s.formInput}>
                <label className={s.formInputLabel}>
                  BaseURI <strong>*</strong>
                </label>
                <input
                  onChange={(e) =>
                    setFormData({ ...formData, baseURI: e.target.value })
                  }
                  value={formData.baseURI}
                  className={s.textInput}
                  name="baseURI"
                  placeholder="https://example.com"
                />
              </div>

              <div className={s.formInput}>
                <label className={s.formInputLabel}>
                  Fee <strong>*</strong>
                </label>
                <input
                  name="fee"
                  placeholder="10"
                  className={s.textInput}
                  type={"number"}
                  onChange={(e) =>
                    setFormData({ ...formData, fee: Number(e.target.value) })
                  }
                  // validate={{ regexp: /^[0-9]$/ }}
                />
              </div>

              <div className={s.formInput}>
                <label className={s.formInputLabel}>
                  Amount <strong>*</strong>
                </label>
                <input
                  placeholder="10"
                  onChange={(e) =>
                    setFormData({
                      ...formData,
                      amount: Number(e.target.value),
                    })
                  }
                  className={s.textInput}
                  name="amount"
                />
              </div>
            </div>
            <button
              className={s.buttonSecondaryYellow}
              onClick={handleBuildContract}
            >
              Create Contract
            </button>
          </form>
          {messageErr && (
            <div className={s.messageContainer}>
              <p className={s.err}>{messageErr}</p>
              <button
                className={s.buttonSecondaryYellow}
                onClick={() => location.reload()}
              >
                Retry
              </button>
            </div>
          )}
          {message && (
            <div className={s.messageContainer}>
              <h1>{message}</h1>
              <Link to={"/factory"} className={s.buttonSecondaryGreen}>
                Go to my collections
              </Link>
            </div>
          )}
        </div>
      )}
    </>
  );
};

export default CreateContract;
