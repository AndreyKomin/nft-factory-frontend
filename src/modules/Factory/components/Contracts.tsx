import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { FactoryContext, TFactoryContext } from "../FactoryContext";
import s from "../../../styles/modules/Factory/Contracts.module.scss";
import briefcaseIcon from "../../../assets/images/briefcase.svg";
import ContractCard from "../../../components/ContractCard/ContractCard";
import LoadingLayout from "../../../components/LoadingLayout";
import cn from "classnames";

const Contracts = () => {
  const {
    data: { contracts, isLoading },
  } = useContext(FactoryContext) as TFactoryContext;

  const isCollectionEmpty = contracts.length === 0;

  return (
    <>
      {isLoading ? (
        <LoadingLayout />
      ) : (
        <div className={cn({ [s.fullPage]: isCollectionEmpty })}>
          <div className={s.subheader}>
            <div className={s.title}>
              <img src={briefcaseIcon} alt={"briefcaseIcon"} />
              <h1>Collections</h1>
            </div>
            {!isCollectionEmpty && (
              <Link to={"create"} className={s.buttonPrimaryColored}>
                Create collection
              </Link>
            )}
          </div>
          <div className={s.contractsList}>
            {isCollectionEmpty ? (
              <div className={s.emptyState}>
                <p>You haven't created any collection yet</p>
                <Link to={"create"} className={s.buttonSecondaryYellow}>
                  Create collection
                </Link>
              </div>
            ) : (
              contracts.map((contract) => <ContractCard contract={contract} />)
            )}
          </div>
        </div>
      )}
    </>
  );
};

export default Contracts;
