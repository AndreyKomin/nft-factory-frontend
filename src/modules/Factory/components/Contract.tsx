/* eslint-disable react-hooks/rules-of-hooks */
import React, { useContext } from "react";
import s from "../../../styles/modules/Factory/Contract.module.scss";
import { Link, useParams } from "react-router-dom";
import LoadingLayout from "../../../components/LoadingLayout";
import { MainContext, TMainContext } from "../../../context/MainContext";
import placeholder from "../../../assets/images/collection-placeholder.png";
import { FactoryContext, TFactoryContext } from "../FactoryContext";
import cn from "classnames";

const Contract = () => {
  const {
    data: { userError, isLoading, contractMeta },
  } = useContext(MainContext) as TMainContext;
  const {
    actions: { setContractAddress },
  } = useContext(FactoryContext) as TFactoryContext;
  //get params from query
  const { contractAddress } = useParams();

  setContractAddress(contractAddress);
  return (
    <>
      {isLoading ? (
        <LoadingLayout />
      ) : userError ? (
        <> {userError}</>
      ) : (
        <div className={s.contract}>
          <div className={s.imagePlaceholder}>
            <img className={s.avatar} src={placeholder} />
            <span className={s.imagePlaceholderText}>No Image</span>
          </div>
          <div>
            <h1>Contract Name: {contractMeta?.contractMetadata?.name}</h1>
            <p>Address: {contractMeta?.address}</p>
            <p>Symbol: {contractMeta?.contractMetadata?.symbol}</p>
            <p>Total Supply: {contractMeta?.contractMetadata?.totalSupply}</p>
            {contractMeta && (
              <>
                <Link
                  className={s.buttonPrimaryColored}
                  to={`/client/${contractAddress}/my-collection`}
                >
                  Manage in Collection
                </Link>
              </>
            )}
          </div>
        </div>
      )}
    </>
  );
};

export default Contract;
