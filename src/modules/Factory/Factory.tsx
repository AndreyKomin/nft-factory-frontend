import { Route, Routes } from "react-router-dom";
import Contracts from "./components/Contracts";
import CreateContract from "./components/CreateContract";
import Contract from "./components/Contract";
import Mint from "../Mint";
import React from "react";
import FactoryProvider from "./FactoryContext";

const Factory = () => (
  <FactoryProvider>
    <Routes>
      <Route path={"/"} element={<Contracts />}></Route>

      {/*contract creation form*/}
      <Route path="/create" element={<CreateContract />} />

      {/*shows all your tokens by contract*/}
      <Route path="/:contractAddress" element={<Contract />} />
      <Route path="/mint" element={<Mint />} />
    </Routes>
  </FactoryProvider>
);

export default Factory;
