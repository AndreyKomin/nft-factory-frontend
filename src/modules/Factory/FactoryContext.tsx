import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import { ethers } from "ethers";
import { FactoryAddress } from "../../config";
import abi from "./abi/Factory.json";
import Api from "../../data/api";
import { MainContext, TMainContext } from "../../context/MainContext";
import { useMutation } from "@tanstack/react-query";
import { IFormState } from "./components/CreateContract";
import to from "await-to-js";

export type TFactoryContext = {
  data: {
    contractMeta: any;
    isLoading: boolean;
    factoryInstance: any;
    contracts: any[];
  };
  actions: {
    setContractAddress: any;
    setLoading: any;
    buildContract: (
      formData: IFormState,
      onSuccess: () => void,
      onError: () => void
    ) => Promise<any>;
  };
};

function getRandomInt(min: number, max: number) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
}

export const FactoryContext = createContext<TFactoryContext | null>(null);

const FactoryProvider = ({ children }: any) => {
  const {
    actions: { setLoading, setUserError, setContractAddress },
    data: { signer, account, isActive, isLoading, contractMeta },
  } = useContext(MainContext) as TMainContext;

  const [factoryInstance, setFactoryInstance] = useState<any>({});
  //all contracts from our api created by this account address(admin)
  const [contracts, setContracts] = useState<any[]>([]);

  const [transaction, setTransaction] = useState(null);

  const { mutateAsync: createContract } = useMutation(Api.createContract);

  useEffect(() => {
    const factory = new ethers.Contract(FactoryAddress, abi.abi, signer);
    setFactoryInstance(factory);
  }, [signer]);

  //all contracts from our api created by this account address(admin)
  const getAllContracts = useCallback(async () => {
    const data = await Api.getContracts(account);
    setContracts(data);
  }, [account]);

  useEffect(() => {
    if (isActive && account) {
      setLoading(true);
      getAllContracts().then(async () => {
        setLoading(false);
      });
    }
  }, [isActive, account]);

  const buildContract = useCallback(
    async (
      formData: IFormState,
      onSuccess: () => void,
      onError: () => void
    ) => {
      let { baseURI } = formData;
      const { standard, name, symbol, fee, amount } = formData;
      // form validation check
      const is1155 = standard === "ERC1155";
      const is721 = standard === "ERC721";
      if (!name || !symbol || !baseURI || !fee || !amount) {
        alert("Fill all fields, please!");
        return;
      }

      baseURI = baseURI.replace(/\/?$/, "/");

      const salt = getRandomInt(1000, 1000000);

      let args: any[] = [];

      if (is721) {
        args = [721, name, symbol, baseURI, account, fee, salt, amount, [], []];
      }

      if (is1155) {
        const ids: number[] = Array.from({ length: Number(amount) }).map(
          (_, i) => i
        );
        const amounts: number[] = ids.map(() => 10);

        args = [
          1155,
          name,
          symbol,
          baseURI,
          account,
          fee,
          salt,
          0,
          ids,
          amounts,
        ];
      }

      setLoading(true);

      const [errorTx, tx]: [any, any] = await to(
        factoryInstance.createContract(...args)
      );
      if (errorTx || !tx) {
        alert("Error during creating contract 1");
        setLoading(false);
        onError();
        return;
      }

      let [errorWait, txInfo]: [any, any] = await to(tx.wait(10));

      if (errorWait && errorWait.code !== "TRANSACTION_REPLACED") {
        alert("Error during creating contract 2");
        setLoading(false);
        onError();
        return;
      }

      if (errorWait && errorWait.code === "TRANSACTION_REPLACED") {
        [errorWait, txInfo] = await to(errorWait.replacement.wait(10));
      }

      // TODO: check errorWait recursively

      //find new contract address
      const deployEvent = txInfo.events.find(
        (item: { event: string }) => item.event === "Deployed"
      );

      // save contract on server
      await createContract({
        name,
        owner: txInfo.from,
        address: deployEvent.args.contractAddress,
        transaction: txInfo.transactionHash,
      });

      setTransaction(txInfo);
      getAllContracts().then();
      onSuccess();
      // setLoading(false);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    },
    [account, factoryInstance]
  );

  // const predictAddress = useCallback(async (formData: IFormState) => {
  //   const { standard, salt } = formData;
  //   let address: string;
  //
  //   if (standard == "ERC1155") {
  //     address = await factoryInstance.predictAddress(1155, salt);
  //     setPredictedAddress(address);
  //   }
  //
  //   if (standard == "ERC721") {
  //     address = await factoryInstance.predictAddress(721, salt);
  //     setPredictedAddress(address);
  //   }
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, []);

  return (
    <FactoryContext.Provider
      value={{
        data: {
          contractMeta,
          isLoading,
          factoryInstance,
          contracts,
        },
        actions: {
          setContractAddress,
          setLoading,
          buildContract,
          // predictAddress,
        },
      }}
    >
      {children}
    </FactoryContext.Provider>
  );
};

export default FactoryProvider;
