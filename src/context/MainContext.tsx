import * as React from "react";
import { createContext, useCallback, useEffect, useState } from "react";
import { useWeb3React } from "@web3-react/core";
import { ethers } from "ethers";
import ConnectWallet from "../components/ConnectWallet/ConnectWallet";
import abiCollection1155 from "../abi/ERC1155.json";
import abiCollection721 from "../abi/ERC721.json";

export type TMainContext = {
  actions: {
    setLoading: (value: boolean) => void;
    setUserError: (value: any) => void;
    getBalance: (value: any) => void;
    setContractAddress: (value: any) => void;
    setContractBaseUri: (value: any) => void;
  };
  data: {
    accBalance: string | undefined;
    userError: boolean | string;
    isLoading: boolean;
    isActive: boolean;
    signer: any;
    account: string | null | undefined;
    contractMeta: any;
    is1155: boolean;
    contractAddress: string;
    collectionInstance: any;
    contractBaseUri: any;
  };
};

export const MainContext = createContext<TMainContext | null>(null);

const MainProvider = ({ children }: any) => {
  //globals
  const [isLoading, setLoadingState] = useState(true);
  const [signer, setSigner] = useState<any>();
  const [userError, setUserError] = useState<boolean | string>(false);
  const [accBalance, setAccBalance] = useState<string>();

  const [collectionInstance, setCollectionInstance] = useState<any>();
  const [contractAddress, setContractAddress] = useState<any>();
  const [is1155, setIs1155] = useState(false);
  const [contractMeta, setContractMeta] = useState<any>();
  const [contractBaseUri, setContractBaseUri] = useState<any>();

  function setLoading(value: boolean) {
    setLoadingState(value);
  }

  const { account, library, active: isActive } = useWeb3React();

  useEffect(() => {
    //setting collection instance
    if (!contractAddress) {
      return;
    }
    const collectionAbi = is1155 ? abiCollection1155.abi : abiCollection721.abi;
    const collection = new ethers.Contract(
      contractAddress,
      collectionAbi,
      signer
    );
    setCollectionInstance(collection);
  }, [signer, is1155, contractAddress, account]);

  const getBaseUri = useCallback(async () => {
    let baseUri;
    if (is1155) {
      baseUri = collectionInstance.uri(Number(0)).then();
    } else {
      baseUri = collectionInstance.baseURI().then();
    }
    return baseUri;
  }, [collectionInstance, is1155]);

  const getContractMeta = useCallback(async () => {
    if (collectionInstance) {
      const name = await collectionInstance.name();
      const symbol = await collectionInstance.symbol();
      // 721 = 0x80ac58cd
      // 1155 = 0xd9b67a26
      const tokenType = await collectionInstance
        .supportsInterface(0xd9b67a26)
        .then((value: boolean) => {
          setIs1155(value);
          return value;
        });
      const baseUri = await getBaseUri();
      setContractMeta({
        address: contractAddress,
        contractMetadata: {
          name,
          symbol,
          tokenType,
          baseUri,
        },
      });
    }
  }, [collectionInstance, contractAddress]);

  //contract meta effect
  useEffect(() => {
    if (!collectionInstance) {
      return;
    }
    getContractMeta().then();
    getBaseUri().then();
  }, [collectionInstance, is1155]);

  const getBalance = useCallback(
    async (provider: any) => {
      provider.getBalance(account).then((bal: any) => {
        const formattedBalance = ethers.utils.formatUnits(bal);
        const value = formattedBalance.toString().split(".");
        const Numvalue =
          value[0] + `,${Math.ceil(Number(value[1]) / 100000000000000)}`;
        setAccBalance(Numvalue);
      });
    },
    [account]
  );

  //project initial load
  useEffect(() => {
    if (!library || !account) {
      return;
    }

    const { provider } = library;
    const ethersProvider = new ethers.providers.Web3Provider(provider);
    getBalance(ethersProvider).then();
    setSigner(ethersProvider.getSigner());
  }, [library, isActive]);

  return (
    <MainContext.Provider
      value={{
        actions: {
          setLoading,
          setUserError,
          getBalance,
          setContractAddress,
          setContractBaseUri,
        },
        data: {
          contractBaseUri,
          contractAddress,
          collectionInstance,
          contractMeta,
          is1155,
          accBalance,
          userError,
          account,
          signer,
          isActive,
          isLoading,
        },
      }}
    >
      {isActive ? children : <ConnectWallet />}
    </MainContext.Provider>
  );
};
export default MainProvider;
