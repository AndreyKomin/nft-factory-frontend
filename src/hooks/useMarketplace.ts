import { useCallback, useState } from "react";

type OrderObject = {
  amount: { _hex: string };
  buyer: string;
  orderId: { _hex: string };
  percentFee: { _hex: string };
  priceWEI: { _hex: string };
  seller: string;
  sellerAccepted: boolean;
  tokenId: { _hex: string };
};
const useMarketplace = ({
  contractAddress,
  activeTokenId,
  marketplaceInstance,
  setLoading,
  is1155,
}: any) => {
  const [orders, setOrders] = useState<any>([]);
  const [ordersByToken, setOrdersByToken] = useState<any>([]);

  const getAllOrders = useCallback(async () => {
    setLoading(true);
    if (!marketplaceInstance) {
      return [];
    }
    const allOrders = await marketplaceInstance.getAllOrders(contractAddress);
    setOrders(allOrders);
    setLoading(false);

    return allOrders;
  }, [marketplaceInstance, contractAddress]);

  const getAllOrdersByToken = useCallback(
    async (tokenId: string) => {
      if (!(orders.length > 0)) {
        await getAllOrders();
        return;
      } else {
        const ordersFiltered = orders.filter(
          (order: { tokenId: { _hex: string } }) => {
            return `${parseInt(order.tokenId._hex)}` === tokenId;
          }
        );
        setOrdersByToken(ordersFiltered);
      }
    },
    [marketplaceInstance, orders, getAllOrders]
  );

  const waitTransaction = async (transaction: any) =>
    transaction
      .wait(10)
      .then(() => {
        getAllOrders().then(() => {
          getAllOrdersByToken(activeTokenId);
        });

        setLoading(false);
      })
      .catch((err: any) => {
        console.log(err);
      })
      .finally((data: any) => {
        console.log(data);
      });

  const startAuction = useCallback(
    async (tokenId: any, amount: any, priceWEI: any, secondsToEnd: any) => {
      setLoading(true);
      const transaction = await marketplaceInstance.addOrder(
        contractAddress,
        tokenId,
        Number(amount),
        priceWEI,
        secondsToEnd
      );
      await waitTransaction(transaction);
      await getAllOrdersByToken(tokenId);
    },

    [marketplaceInstance, contractAddress]
  );

  const addOrder = useCallback(
    async (formValue: { priceWEI: any; amount: any }, tokenId: any) => {
      setLoading(true);
      const { priceWEI, amount } = formValue;
      const params = is1155
        ? [contractAddress, Number(tokenId), Number(amount), priceWEI]
        : [contractAddress, Number(tokenId), priceWEI];
      const transaction = await marketplaceInstance.addOrder(...params);
      await waitTransaction(transaction);
      await getAllOrdersByToken(tokenId);
    },
    [marketplaceInstance, contractAddress]
  );

  const redeemOrder = useCallback(
    async (id: number, price: number) => {
      console.log(id, price);
      setLoading(true);
      const transaction = await marketplaceInstance.redeemOrder(
        contractAddress,
        id,
        {
          value: price,
        }
      );
      await waitTransaction(transaction);
      await getAllOrdersByToken(`${id}`);
    },
    [marketplaceInstance, contractAddress]
  );

  const removeOrder = useCallback(
    async (id: number) => {
      setLoading(true);
      const transaction = await marketplaceInstance.removeOrder(
        contractAddress,
        id
      );
      await waitTransaction(transaction);
      await getAllOrdersByToken(`${id}`);
    },
    [marketplaceInstance, contractAddress]
  );

  const declineOrder = useCallback(
    async (id: number) => {
      setLoading(true);
      const transaction = await marketplaceInstance.declineOrder(
        contractAddress,
        id
      );
      await waitTransaction(transaction);
      await getAllOrdersByToken(`${id}`);
    },
    [marketplaceInstance, contractAddress]
  );

  const initializeOrder = useCallback(
    async (id: number) => {
      setLoading(true);
      const transaction = await marketplaceInstance.initializeOrder(
        contractAddress,
        id
      );
      await waitTransaction(transaction);
      await getAllOrdersByToken(`${id}`);
      setLoading(false);
    },
    [marketplaceInstance, contractAddress]
  );

  const acceptOrder = useCallback(
    async (id: number) => {
      setLoading(true);
      const transaction = await marketplaceInstance.acceptOrder(
        contractAddress,
        id,
        true
      );
      await waitTransaction(transaction);
      await getAllOrdersByToken(`${id}`);
    },
    [marketplaceInstance, contractAddress]
  );

  return {
    actionsMarketplace: {
      getAllOrders,
      getAllOrdersByToken,
      startAuction,
      addOrder,
      removeOrder,
      redeemOrder,
      declineOrder,
      initializeOrder,
      acceptOrder,
    },
    dataMarketplace: {
      orders,
      ordersByToken,
    },
  };
};

export default useMarketplace;
