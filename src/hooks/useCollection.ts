import { useCallback, useState } from "react";

const useCollection = ({
  collectionInstance,
  setLoading,
  is1155,
  account,
}: any) => {
  const [ownerOfToken, setOwnerOfToken] = useState<boolean>(false);
  const [tokenAmount, setTokenAmount] = useState<number>(0);

  const getOwner = useCallback(
    async (tokenId: any) => {
      setLoading(true);
      if (!collectionInstance) {
        return [];
      }
      const params = is1155 ? [account, Number(tokenId)] : [account];
      const owner = await collectionInstance.balanceOf(...params);
      setTokenAmount(parseInt(owner._hex));
      setOwnerOfToken(parseInt(owner._hex) !== 0);
      setLoading(false);
    },
    [collectionInstance, account]
  );

  return {
    collectionData: {
      ownerOfToken,
      tokenAmount,
    },
    collectionActions: {
      getOwner,
    },
  };
};

export default useCollection;
