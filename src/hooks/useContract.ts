import { useCallback, useEffect, useState } from "react";
import abiFactory from "../abi/NFTFactory.json";

type TContract = {
  hasLoaded: boolean;
  instance: any;
  data: any;
};

const getData = async (contract: any, params: string[]) => {
  const data: any = {};

  await Promise.all(
    params.map(async (param: string) => {
      data[param] = await contract[param]().call({ _isConstant: true });
    })
  );

  return data;
};

const useContract = (
  trc20ContractAddress?: string,
  getParams: string[] = []
): TContract => {
  const [instance, setInstance] = useState({});
  const [data, setData] = useState({});
  const [hasLoaded, setLoaded] = useState(false);

  const getContract = useCallback(async () => {
    try {
      const contract = await window.tronWeb.contract().at(trc20ContractAddress);

      if (!contract.abiFactory.abi.length) {
        contract.loadAbi(abiFactory.abi);
      }

      const collectedData = await getData(contract, getParams);

      setInstance(contract);
      setData(collectedData);
      setLoaded(true);
    } catch (error) {
      console.error("trigger smart contract error", error);
    }
  }, [trc20ContractAddress, getParams]);

  useEffect(() => {
    if (!trc20ContractAddress) return;

    setLoaded(false);
    getContract().then(() => {});
    // eslint-disable-next-line
  }, []);

  return {
    hasLoaded,
    instance,
    data,
  };
};

export default useContract;
