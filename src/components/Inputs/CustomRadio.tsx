import s from "./Inputs.module.scss";
import { useEffect, useState } from "react";

const CustomRadio = ({
  checked,
  value,
  label,
  onChangeHandle,
}: {
  checked: boolean;
  label: string;
  value: any;
  onChangeHandle: any;
}) => {
  const [state, setState] = useState<boolean>(false);
  const id = `id-${label.replace(/\s/, "-")}`;

  useEffect(() => {
    setState(checked);
    return () => {};
  });
  const handleChange = (stateVal: boolean) => {
    setState(stateVal);
    onChangeHandle(value);
  };
  return (
    <div className={s.radioBtn}>
      <div className={s.radioCircle} onClick={() => handleChange(!state)}>
        {checked && <div className={s.checker}></div>}
      </div>
      <input
        id={id}
        className={s.radioBtnInput}
        onClick={() => handleChange(!state)}
        type={"radio"}
      />
      <label htmlFor={id} className={s.label}>
        {label}
      </label>
    </div>
  );
};
export default CustomRadio;
