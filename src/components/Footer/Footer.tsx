import s from "./Footer.module.scss";
import logo from "../../assets/images/logo.svg";
import telegram from "../../assets/images/social/telegram.svg";
import twitter from "../../assets/images/social/twitter.svg";
import discord from "../../assets/images/social/discord.svg";
import github from "../../assets/images/social/github.svg";
import { Link } from "react-router-dom";
import React from "react";

const Footer = () => {
  return (
    <footer className={s.footer}>
      <div className={s.logoSection}>
        <Link to={"/factory"} className={s.logo}>
          <img className={s.logo} src={logo} alt="Logo" />
        </Link>
      </div>
      <div className={s.center}>
        <div className={s.linksSection}>
          <h5 className={s.title}>Quick Links</h5>
          <ul className={s.links}>
            <li>
              <a
                href="https://www.npmjs.com/package/nft-factory-javascript-sdk"
                target="_blank"
              >
                Developer Package & Docs
              </a>
            </li>
            <li>
              <Link to={"/roadmap"}>Roadmap</Link>
            </li>
            <li>
              <Link to={"/white-paper"}>White paper</Link>
            </li>
            <li>
              <Link to={"/about-us"}>About us</Link>
            </li>
          </ul>
        </div>
      </div>
      <div className={s.social}>
        <a href="https://twitter.com/NonFungible_F" target="_blank">
          <img className={s.socialIcon} src={twitter} alt="Twitter" />
        </a>
        <a href="https://t.me/nonfungiblefactory" target="_blank">
          <img className={s.socialIcon} src={telegram} alt="Telegram" />
        </a>
        <a href="#">
          <img className={s.socialIcon} src={discord} alt="Discord" />
        </a>
        <a href="https://github.com/shareconomy" target="_blank">
          <img className={s.socialIcon} src={github} alt="Github" />
        </a>
      </div>
    </footer>
  );
};
export default Footer;
