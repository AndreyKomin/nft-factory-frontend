import { useWeb3React } from "@web3-react/core";
import { Button, Layer, Image, Text, Box, Heading } from "grommet";
import s from "./ConnectWallet.module.scss";
import React, { useEffect, useState } from "react";
import { connectors } from "../../data/connectors";
import cbw from "../../assets/images/cbw.png";
import wc from "../../assets/images/wc.png";
import mm from "../../assets/images/mm.png";
import { networkParams } from "../../config";
import logo from "../../assets/images/logo.svg";

declare let window: any;

export const truncateAddress = (address: string) => {
  if (!address) return "No Account";
  const match = address.match(
    /^(0x[a-zA-Z0-9]{2})[a-zA-Z0-9]+([a-zA-Z0-9]{2})$/
  );
  if (!match) return address;
  return `${match[1]}…${match[2]}`;
};

export const toHex = (num: any) => {
  const val = Number(num);
  return "0x" + val.toString(16);
};

function ConnectWallet() {
  const { library, chainId, account, activate, deactivate, active } =
    useWeb3React();
  const [show, setShow] = useState(false);
  const [error, setError] = useState("");
  const [network, setNetwork] = useState<any>(undefined);

  const handleNetwork = (e: any) => {
    const id = e.target.value;
    setNetwork(Number(id));
  };

  const switchNetwork = async () => {
    try {
      await library.provider.request({
        method: "wallet_switchEthereumChain",
        params: [{ chainId: toHex(network) }],
      });
    } catch (switchError: any) {
      if (switchError.code === 4902) {
        try {
          await library.provider.request({
            method: "wallet_addEthereumChain",
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            params: [networkParams[toHex(network)]],
          });
        } catch (e: any) {
          setError(e);
        }
      }
    }
  };

  const refreshState = () => {
    window.localStorage.setItem("provider", undefined);
    setNetwork("");
  };

  const disconnect = () => {
    refreshState();
    deactivate();
  };

  useEffect(() => {
    const provider = window.localStorage.getItem("provider");
    if (provider) activate(connectors[provider]).then();
  }, [activate]);

  const setProvider = (type: any) => {
    window.localStorage.setItem("provider", type);
  };

  const closeModal = () => setShow(false);

  return (
    <div className={s.connectWallet}>
      <img className={s.logo} src={logo} alt="Logo" />
      <h1>Use Mainnet, Please!</h1>
      <h3>To start interacting with the platform connect your wallet.</h3>
      <button className={s.buttonSecondaryYellow} onClick={() => setShow(true)}>
        Connect Wallet
      </button>
      {show && (
        <Layer
          className={s.modal}
          background={{ color: "#2c2d31" }}
          onEsc={() => setShow(false)}
          onClickOutside={() => setShow(false)}
        >
          <Button
            onClick={() => {
              activate(connectors.coinbaseWallet).then();
              setProvider("coinbaseWallet");
              closeModal();
            }}
          >
            <Image
              src={cbw}
              alt="Coinbase Wallet Logo"
              width={25}
              height={25}
            />
            <Text>Coinbase Wallet</Text>
          </Button>
          <Button
            onClick={() => {
              activate(connectors.walletConnect).then();
              setProvider("walletConnect");
              closeModal();
            }}
          >
            <Image src={wc} alt="Wallet Connect Logo" width={26} height={26} />
            <Text>Wallet Connect</Text>
          </Button>
          <Button
            onClick={() => {
              activate(connectors.injected).then();
              setProvider("injected");
              closeModal();
            }}
          >
            <Image src={mm} alt="Metamask Logo" width={25} height={25} />
            <Text>Metamask</Text>
          </Button>
          <button className={s.close} onClick={closeModal}>
            Close
          </button>
        </Layer>
      )}
    </div>
  );
}

export default ConnectWallet;
