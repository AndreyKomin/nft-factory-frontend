import { Spinner } from "grommet";
import styles from "./LoadingLayout.module.css";

const LoadingLayout = () => {
  return (
    <div className={styles.root}>
      <Spinner color={"#ffde11"} />
    </div>
  );
};

export default LoadingLayout;
