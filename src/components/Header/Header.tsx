import { ethers, utils } from "ethers";
import s from "./Header.module.scss";
import logo from "../../assets/images/logo.svg";
import user from "../../assets/images/user.svg";
import React, { useContext, useState } from "react";
import { useWeb3React } from "@web3-react/core";
import ConnectWallet from "../ConnectWallet/ConnectWallet";
import { matchPath, useLocation, NavLink, Link } from "react-router-dom";
import { MainContext, TMainContext } from "../../context/MainContext";

const formatAddress = (address?: string | null) => {
  if (!address) return "";
  const symbolsFirst = 5;
  const symbolsLast = 4;

  const first = address.slice(0, symbolsFirst);
  const last = address.slice(address.length - symbolsLast, address.length);

  return `${first}...${last}`;
};

const Header = () => {
  const data = useWeb3React();
  const { activate, deactivate, active, chainId, library, account } = data;
  const loc = useLocation();
  const {
    data: { accBalance },
  } = useContext(MainContext) as TMainContext;
  const matchPath1 = matchPath(
    { path: "/factory", caseSensitive: false, end: false },
    loc.pathname
  );
  const matchPath2 = matchPath(
    { path: "/client/:contractAddress", caseSensitive: false, end: false },
    loc.pathname
  );
  return (
    <header className={s.header}>
      <div className={s.headerContainer}>
        <Link to={"/factory"} className={s.logo}>
          <img className={s.logo} src={logo} alt="Logo" />
        </Link>

        <nav className={s.nav}>
          {matchPath1 === null && (
            <>
              <NavLink
                to={`/client/${matchPath2?.params.contractAddress}/marketplace`}
              >
                Marketplace
              </NavLink>
              <NavLink
                to={`/client/${matchPath2?.params.contractAddress}/my-collection`}
              >
                My Collection
              </NavLink>
            </>
          )}
        </nav>
        {active ? (
          <div className={s.account}>
            <div className={s.info}>
              <span className={s.address}>{formatAddress(account)}</span>
              <span>Balance: {accBalance}</span>
            </div>
            <img className={s.user} src={user} alt="Logo" />
          </div>
        ) : (
          <ConnectWallet />
        )}
      </div>
    </header>
  );
};

export default Header;
