import { Link } from "react-router-dom";
import s from "./ContractCard.module.scss";
import placeholder from "../../assets/images/collection-placeholder.png";

const ContractCard = ({ contract }: { contract: any }) => {
  return (
    <Link className={s.contract} to={contract.address || "/"}>
      {!contract.image && (
        <div className={s.imagePlaceholder}>
          <img className={s.avatar} src={placeholder} />
          <span className={s.imagePlaceholderText}>No Image</span>
        </div>
      )}
      {contract.image && <img className={s.avatar} src={placeholder} />}
      <div className={s.info}>
        <h2 className={s.name}>{contract.name}</h2>
        <div className={s.statistics}>
          <div className={s.stat}>
            <span className={s.label}>Items:</span>
            <span className={s.value}>0</span>
          </div>
          <div className={s.stat}>
            <span className={s.label}>Owners:</span>
            <span className={s.value}>1</span>
          </div>
          <div className={s.stat}>
            <span className={s.label}>Total volume:</span>
            <span className={s.value}>0</span>
          </div>
          <div className={s.stat}>
            <span className={s.label}>Best offer:</span>
            <span className={s.value}>0</span>
          </div>
          <div className={s.stat}>
            <span className={s.label}>Floor price:</span>
            <span className={s.value}>0</span>
          </div>
        </div>
      </div>
    </Link>
  );
};
export default ContractCard;
