import s from "./TokenCard.module.scss";
import { Link } from "react-router-dom";
import React from "react";

const TokenCard = ({ data }: { data: any }) => {
  const { imgSrc, link, amount, tokenId } = data;
  console.log(data);
  return (
    <Link to={link} className={s.tokenCard}>
      <div className={s.imageContainer}>
        <img className={s.image} src={imgSrc} />
        {amount && <div className={s.amount}>{amount}</div>}
      </div>

      <h3 className={s.name}>NFT #{tokenId}</h3>
    </Link>
  );
};
export default TokenCard;
