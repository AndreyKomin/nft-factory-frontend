import client from "./axios";
import axios from "axios";

const createContract = (data: any) => {
  return client.post("/contracts", data);
};

const getContracts = async (accountAddress: string | null | undefined) => {
  const response = await client.get("/contracts", {
    params: { accountAddress },
  });
  return response.data;
};

export default {
  createContract,
  getContracts,
};
