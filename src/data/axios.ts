import axios from "axios";

// const baseURL = "http://localhost:8000";
const baseURL = "https://non-fungible-factory.herokuapp.com";

const createClient = () =>
  axios.create({
    baseURL,
  });

export default createClient();
export { createClient };
