import axios from "./axios";
import { AlchemyApiKey, AlchemyNetwork } from "../config";
import to from "await-to-js";

export const getNFTMetadata = async (
  contractAddress: string | undefined,
  tokenId: number | null
) => {
  const response = await axios.get(
    `${AlchemyNetwork}/${AlchemyApiKey}/getNFTMetadata`,
    {
      params: {
        contractAddress,
        tokenId,
      },
    }
  );

  return response.data;
};

export const getNFTs = async (
  owner: string | null | undefined,
  contractAddresses: string[]
): Promise<{ ownedNfts: [] }> => {
  const response = await axios.get(
    `${AlchemyNetwork}/${AlchemyApiKey}/getNFTs`,
    {
      params: {
        owner,
        contractAddresses,
      },
    }
  );

  return response.data;
};

export const getOwnerOfToken = async (
  contractAddress: string | null | undefined,
  tokenId: any
): Promise<{ owners: string[] }> => {
  const response = await axios.get(
    `${AlchemyNetwork}/${AlchemyApiKey}/getOwnersForToken`,
    {
      params: {
        contractAddress,
        tokenId,
      },
    }
  );

  return response.data;
};
