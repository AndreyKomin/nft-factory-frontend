import { InjectedConnector } from "@web3-react/injected-connector";
import { WalletConnectConnector } from "@web3-react/walletconnect-connector";
import { WalletLinkConnector } from "@web3-react/walletlink-connector";

const INFURA_KEY = "dc149e32141749b0bc87b664a42aa087";

const coinbaseWallet = new WalletLinkConnector({
  url: "https://polygon-mumbai.infura.io/v3/dc149e32141749b0bc87b664a42aa087",
  appName: "Web3-react Demo",
  supportedChainIds: [1, 3, 4, 5, 42, 80001],
});

// url - gets from Infura https://infura.io/dashboard by setting your credit card and adding 'PoS' add-on
const walletConnect = new WalletConnectConnector({
  rpc: "https://polygon-mumbai.infura.io/v3/dc149e32141749b0bc87b664a42aa087",
  bridge: "https://bridge.walletconnect.org",
  qrcode: true,
});

const injected = new InjectedConnector({
  supportedChainIds: [1, 3, 4, 5, 42, 80001, 137],
});

// const walletConnect = new WalletConnectConnector({
//   // rpc: `https://mainnet.infura.io/v3/${INFURA_KEY}`,
//   rpc: `https://polygon-mumbai.infura.io/v3/${INFURA_KEY}`,
//   bridge: "https://bridge.walletconnect.org",
//   qrcode: true,
// });

// const coinbaseWallet = new WalletLinkConnector({
//   // url: `https://mainnet.infura.io/v3/${INFURA_KEY}`,
//   url: `https://polygon-mumbai.infura.io/v3/${INFURA_KEY}`,
//   appName: "web3-react-demo",
// });

export const connectors: any = {
  injected,
  walletConnect,
  coinbaseWallet,
};
